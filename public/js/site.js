$(()=> {
    const prepareRenderer = () => {
        let canvas = getCanvas();
        let renderer = new THREE.WebGLRenderer({antialias: true, canvas});
        renderer.setSize(settings.window.width, settings.window.height);
        $('.app-container').append(renderer.domElement);

        return renderer;
    }

    const getCanvas = () => {
        return document.querySelector('.canvas-container');
    }

    const getDirectionalLight =  () => {
        let light = new THREE.DirectionalLight(settings.directionalLight.color, settings.directionalLight.intensity);
        light.position.set(-1, 2, 4);

        return light;
    }

    const getAmbientLight = () => {
        return new THREE.AmbientLight(settings.ambientLight.color, settings.ambientLight.intensity);
    }

    let textureLoader = new THREE.TextureLoader();
    let texture1 = textureLoader.load('//cdn.rawgit.com/mrdoob/three.js/master/examples/textures/brick_diffuse.jpg');
    let texture2 = textureLoader.load('//cdn.rawgit.com/mrdoob/three.js/master/examples/textures/floors/FloorsCheckerboard_S_Diffuse.jpg');

    const getCube = () => {
        let geometry = new THREE.BoxGeometry(settings.box.size.width, settings.box.size.height, settings.box.size.depth); // object containing points (vertices) and fill (faces) of the cube
        let material = new THREE.MeshPhongMaterial({color: settings.box.color, map: texture1}); // material covering the box
        return new THREE.Mesh(geometry, material); // object that takes geometry and applies the material to it, it is what we can move around and so on
    }

    const getCamera = () => {
        let camera = new THREE.PerspectiveCamera(settings.camera.fov, settings.window.aspect_ratio, settings.camera.near, settings.camera.far);
        camera.position.set(0, 2, 5); // moving the camera backwards in z axis, away from the box at (0, 0, 0)
    
        return camera;
    }

    const getScene = () => {
        return new THREE.Scene();
    }

    const getOBJLoader = () => {
        return new THREE.OBJLoader();
    }

    const getGLTFLoader = () => {
        return new THREE.GLTFLoader();
    }

    const getControls = () => {
        let controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.minDistance = 1;
        controls.maxDistance = 1000;
        
        return controls;
    }

    const setCubeTextureToBrick = () => {
        cube.material.map = texture1;
        texture2.dispose();
    }

    const setCubeTextureToTiles = () => {
        cube.material.map = texture2;
        texture1.dispose();
    }

    const getPlatform = () => {
        let geometry = new THREE.CylinderGeometry(settings.platform.size.top_radius, settings.platform.size.bottom_radius, settings.platform.size.height, settings.platform.size.top_vertices, settings.platform.size.sides_vertices);
        let material = new THREE.MeshPhongMaterial({color: settings.platform.color});

        return new THREE.Mesh(geometry, material);
    }

    let settings = {
        modelPath: 'gltf/chair/chair_project.glb',
        camera: {
            fov: 75,
            near: 0.1,
            far: 1000,
        },
        window: {
            width: window.innerWidth,
            height: window.innerHeight,
            aspect_ratio: window.innerWidth/window.innerHeight,
        },
        box: {
            size: {
                width: 1,
                height: 1,
                depth: 1,
            },
            color: '#e08283',
        },
        platform: {
            size: {
                top_radius: 8,
                bottom_radius: 8,
                height: 0.5,
                top_vertices: 50,
                sides_vertices: 50,
            },
            color: '#bdc3c7',
        },
        directionalLight: {
            color: '#ffffff',
            intensity: 0.3,
        },
        ambientLight: {
            color: '#ffffff',
            intensity: 0.5,
        },
    }

    let scene = getScene();
    scene.background = new THREE.Color('#6c7a89');

    // let loader = getOBJLoader();
    // loader.load('/models/obj/cow.obj', object => {
    //     scene.add(object);
    // });

    let model;

    let loader = getGLTFLoader();
    loader.load(`models/${settings.modelPath}`, gltf => {

        // let texture = textureLoader.load ('/models/gltf/chair/textures/WoodFineDark004/Regular/3K/WoodFineDark004_COL_3K.jpg');
        // gltf.scene.children[6].material.map = texture;

        // 8, 9, 10, 11, 12, 13, 14 - guziki
        // 7 - niepełna rama krzesła
        // 15 - oparcie krzesła
        // 16 - siedzisko krzesła
        // 4 - horyzontalna belka krzesła
        // 5 - horyzontalna belka krzesła 2
        // 6 - podłoga
        let skeletonTexture = textureLoader.load('/models/gltf/chair/textures/WoodFineDark004/REGULAR/3K/WoodFineDark004_COL_3K.jpg');
        let pillowTexture = textureLoader.load('/models/gltf/chair/textures/pillow/2823451EFA4648AFA69EA108D43C415B.jpg');
        let floorTexture = textureLoader.load('/models/gltf/chair/textures/WoodFineDark004/REGULAR/3K/WoodFineDark004_COL_3K.jpg');

        for (let child of gltf.scene.children) {
            if (child.type === "Mesh") {
                child.material = new THREE.MeshPhongMaterial();

                console.log(child.name);
                if (child.name === 'chair_skeleton') {
                    child.material.map = skeletonTexture;
                }

                if (child.name === "bracket_side001") {
                    child.material.map = skeletonTexture;
                }

                if (child.name === "bracket_front") {
                    child.material.map = skeletonTexture;
                }

                if (child.name === "pillow_back") {
                    child.material.map = pillowTexture;
                }

                if (child.name === "pillow_sit") {
                    child.material.map = pillowTexture;
                }

                if (child.name === 'floor') {
                    child.material.map = floorTexture;
                }
            }
        }

        gltf.scene.children[6].material = new THREE.MeshPhongMaterial();
        scene.add(gltf.scene); 
        console.log(gltf);

        // model = gltf.scene.children[0];
        // model.position.y = 1;
        // scene.add(model);
    }, xhr => {
        console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, error => {
        console.log(`an error occured! ${error}`);
    });

    let camera = getCamera();
    let renderer = prepareRenderer();
    let directionalLight = getDirectionalLight();
    let ambientLight = getAmbientLight();

    let cube = getCube();
    cube.position.y = settings.box.size.height/2;

    let platform = getPlatform();
    let platformTexture = textureLoader.load('/textures/pavement.jpg');

    platform.position.y = -settings.platform.size.height;
    platform.material.map = platformTexture;

    scene.add(directionalLight);
    scene.add(ambientLight);
    //scene.add(cube); // by default it adds the object at (0, 0, 0)
    //scene.add(platform);

    controls = getControls();

    let iteration = 0;
    setInterval(() => {
        iteration % 2 === 0 ? setCubeTextureToBrick() : setCubeTextureToTiles();
        iteration++;
    }, 3000);

    let direction = 1;
    const animate = () => {
        requestAnimationFrame(animate);

        if (model !== undefined) {
            if (model.position.x >= 5 || model.position.x <= -5) {
                direction *= -1;
            }

            model.position.x += 0.01 * direction;

            camera.lookAt(new THREE.Vector3(model.position.x, model.position.y, model.position.z));
        }

        directionalLight.position.set(camera.position.x, camera.position.y, camera.position.z);
        directionalLight.target = new THREE.Object3D();

        renderer.render(scene, camera);
    }

    animate();
});

